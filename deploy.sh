#!/bin/bash

set -e

PROJECTS=(
        AccountManagement_MS
        Customer_MS
        Manager_MS
        Merchant_MS
        Payment_Report_MS
        ReportGeneration_MS
        TokenManagement_MS
)

build_all_projects()
{
        for project in ${PROJECTS[*]}; do
                echo "=========Building $project========="
                pushd "../$project"
                mvn clean
                chmod +x build.sh
                ./build.sh
                popd
        done
}

run_all_tests()
{
        for project in ${PROJECTS[*]}; do
                echo "=========Testing $project========="
                pushd "../$project"
                mvn test -DskipTests=false
                popd
        done
}

#build_all_projects
trap '[ "$?" -eq 0 ] || docker-compose down -rmi all' EXIT
docker-compose up -d --build
sleep 30
mvn test
docker-compose down --rmi all

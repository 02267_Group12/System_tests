package entities;


import java.util.Objects;

public class Token {

    // UUID.randomUUID().toString()
    boolean valid;
    String id;

    public Token() {
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id);
    }

    public boolean equals(Token token)
    {
        return this.id.equals(token.id);
    }

    public void invalidate(){
        valid = false;
    }
}


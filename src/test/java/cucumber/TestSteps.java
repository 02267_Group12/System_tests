package cucumber;

import dtu.ws.fastmoney.Account;
import dtu.ws.fastmoney.Transaction;
import dtu.ws.fastmoney.User;
import entities.DTUPayAccount;
import entities.DTUTransaction;
import entities.PaymentData;
import entities.MerchantTransaction;
import entities.Token;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import clients.*;
import org.junit.jupiter.api.Assertions;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.List;

public class TestSteps{
    User customer;
    User merchant;
    User actor;
    List<Token> tokens;
    BankAccountClient bank = new BankAccountClient();
    CustomerClient customerClient = new CustomerClient();
    MerchantClient merchantClient = new MerchantClient();
    ManagerClient managerClient = new ManagerClient();
    Response paymentResponse;
    String cid;
    String mid;
    String paymentDescription = "end-to-end test transaction";

    boolean successful;
    Transaction lastTransaction;
    PaymentData paymentData = new PaymentData();

    @Given("the customer {string} {string} with CPR {string} has a bank account")
    public void theCustomerWithCPRHasABankAccount(String firstName, String lastName, String cpr) {
        customer = new User();
        customer.setFirstName(firstName);
        customer.setLastName(lastName);
        customer.setCprNumber(cpr);
    }

    @And("the balance of the customer account is {int}")
    public void theBalanceOfTheCustomerAccountIs(int amount) {
        bank.createBankAccount(customer.getFirstName(), customer.getLastName(), customer.getCprNumber(), new BigDecimal(amount));
    }

    @And("the customer is registered with DTUPay")
    public void theCustomerIsRegisteredWithDTUPay() {
        Response response = customerClient.registerCustomer(customer);
        Assertions.assertEquals(200, response.getStatus(), "Failed to register customer");
        String path = response.getLocation().getPath();
        cid = path.substring(path.lastIndexOf('/')+1);
    }

    @And("the merchant {string} {string} with CPR {string} has a bank account")
    public void theMerchantWithCPRNumberHasABankAccount(String firstName, String lastName, String cpr) {
        merchant = new User();
        merchant.setFirstName(firstName);
        merchant.setLastName(lastName);
        merchant.setCprNumber(cpr);
    }
    @And("the balance of the merchant account is {int}")
    public void theBalanceOfTheMerchantAccountIs(int amount) {
        bank.createBankAccount(merchant.getFirstName(), merchant.getLastName(), merchant.getCprNumber(), new BigDecimal(amount));
    }

    @And("the merchant is registered with DTUPay")
    public void theMerchantIsRegisteredWithDTUPay() {
        Response response = merchantClient.registerNewMerchant(merchant);
        Assertions.assertEquals(200, response.getStatus(), "Failed to register merchant");
        String path = response.getLocation().getPath();
        mid = path.substring(path.lastIndexOf('/')+1);
    }

    @And("the customer has {int} valid tokens")
    public void theCustomerHasValidTokens(int numberOfTokens) {
        Response response = customerClient.createTokens(cid, numberOfTokens);
        Assertions.assertEquals(200, response.getStatus(), "Failed to get tokens");
        tokens = response.readEntity(new GenericType<List<Token>>(){});
        Assertions.assertNotEquals(null, tokens, "Failed to add tokens to customer");
        Assertions.assertEquals(numberOfTokens, tokens.size(), "Valid tokens size does not match");
    }

    @When("the merchant initiates a payment for {int} kr by the customer")
    public void theMerchantInitiatesAPaymentForKrByTheCustomer(int amount) {
        var merchantTransaction = new MerchantTransaction();
        merchantTransaction.setTokenId(tokens.get(0).getId());
        merchantTransaction.setMid(mid);
        merchantTransaction.setDescription(paymentDescription);
        merchantTransaction.setAmount(new BigDecimal(amount));
        merchantTransaction.setMonth(1);
        paymentResponse = merchantClient.payToMerchant(merchantTransaction);
    }

    @Then("the payment is successful")
    public void thePaymentIsSuccessful() {
        Assertions.assertEquals(200, paymentResponse.getStatus(), "Payment is not successful");
    }

    @And("the balance of the customer at the bank is {int} kr")
    public void theBalanceOfTheCustomerAtTheBankIsKr(int balance) {
        Account customerAccount = bank.getAccountByCprNumber(customer.getCprNumber());
        Assertions.assertEquals(balance, customerAccount.getBalance().intValue());
    }

    @And("the balance of the merchant at the bank is {int} kr")
    public void theBalanceOfTheMerchantAtTheBankIsKr(int balance) {
        Account merchantAccount = bank.getAccountByCprNumber(merchant.getCprNumber());
        Assertions.assertEquals(balance, merchantAccount.getBalance().intValue());
    }

    @And("the customer report shows the transaction")
    public void theCustomerReportShowsTheTransaction() {
        var response = customerClient.getCustomerReport(cid);
        Assertions.assertEquals(200, response.getStatus());
        List<DTUTransaction> transactions = response.readEntity(new GenericType<List<DTUTransaction>>() {});
        Assertions.assertTrue(transactions.stream().anyMatch(t -> t.getDescription().equals(paymentDescription)));
    }

    @And("the merchant report shows the transaction")
    public void theMerchantReportShowsTheTransaction() {
        Response response = merchantClient.getMerchantReport(mid);
        Assertions.assertEquals(200, response.getStatus());
        var transactions = response.readEntity(new GenericType<List<MerchantTransaction>>(){});

    }

    @And("the manager report shows the transaction")
    public void theManagerReportShowsTheTransaction() {
        Response response = managerClient.getReport();
        Assertions.assertEquals(200, response.getStatus());
        List<DTUTransaction> transactions = response.readEntity(new GenericType<List<DTUTransaction>>(){});
        Assertions.assertTrue(transactions.stream().anyMatch(t -> t.getDescription().equals(paymentDescription)));
    }




    @When("updating merchant to {string} {string} with CPR {string}")
    public void updatingMerchantToWithCPR(String firstName, String lastName, String cprNumber) {
        User merchant = new User();
        merchant.setCprNumber(cprNumber);
        merchant.setFirstName(firstName);
        merchant.setLastName(lastName);
        managerClient.updateMerchantAccount(mid,merchant);
    }

    @When("updating customer to {string} {string} with CPR {string}")
    public void updatingCustomerToWithCPR(String firstName, String lastName, String cprNumber) {
        User customer = new User();
        customer.setCprNumber(cprNumber);
        customer.setFirstName(firstName);
        customer.setLastName(lastName);
        managerClient.updateCustomerAccount(cid,customer);
    }


    @Then("customer {string} {string} with CPR {string} is found")
    public void customerWithCPRIsFound(String firstName, String lastName, String cprNumber) {
        Response response = managerClient.getCustomerAccount(cid);
        Assertions.assertEquals(200, response.getStatus(), "Failed to ask for account of customer");
        customer = response.readEntity(new GenericType<DTUPayAccount>(){}).getUser();
        Assertions.assertTrue(customer.getCprNumber().equals(cprNumber)
                && customer.getFirstName().equals(firstName) && customer.getLastName().equals(lastName));
    }

    @Then("merchant {string} {string} with CPR {string} is found")
    public void merchantWithCPRIsFound(String firstName, String lastName, String cprNumber) {
        Response response = managerClient.getMerchantAccount(mid);
        Assertions.assertEquals(200, response.getStatus(), "Failed to ask for account of customer");
        merchant = response.readEntity(new GenericType<DTUPayAccount>(){}).getUser();
        Assertions.assertTrue(merchant.getCprNumber().equals(cprNumber)
                && merchant.getFirstName().equals(firstName) && merchant.getLastName().equals(lastName));
    }

    @When("deleting customer {string} {string} with CPR {string}")
    public void deletingCustomerWithCPR(String firstName, String lastName, String cprNumber) {
        Response response = managerClient.deleteCustomerAccount(cid);
        Assertions.assertEquals(202, response.getStatus(), "Failed to delete customer");
    }



    @When("deleting merchant {string} {string} with CPR {string}")
    public void deletingMerchantWithCPR(String firstName, String lastName, String cprNumber) {
        Response response = managerClient.deleteMerchantAccount(mid);
        Assertions.assertEquals(202, response.getStatus(), "Failed to delete merchant");
    }

    @Then("customer {string} {string} with CPR {string} is not found")
    public void customerWithCPRIsNotFound(String firstName, String lastName, String cprNumber) {
        Response response = managerClient.getCustomerAccount(cid);
        Assertions.assertNotEquals(200, response.getStatus(), "Customer is still found");
    }

    @Then("merchant {string} {string} with CPR {string} is not found")
    public void merchantWithCPRIsNotFound(String firstName, String lastName, String cprNumber) {
        Response response = managerClient.getMerchantAccount(mid);
        Assertions.assertNotEquals(200, response.getStatus(), "Merchant is still found");
    }

    @And("cannot access to tokens")
    public void cannotAccessToTokens() {
        Response response = customerClient.getTokensForCustomer(cid);
        Assertions.assertNotEquals(200, response.getStatus(), "Can access tokens still");
    }

    @After
    public void removeAccounts() {
        try{
            managerClient.deleteMerchantAccount(mid);
        } catch (Exception e){

        }
        try{
            managerClient.deleteCustomerAccount(cid);
        } catch (Exception e){

        }
        System.out.println("Delete accounts after tests...");
        try {
            String CPR_1 = customer.getCprNumber();
            String id_1 = bank.getAccountByCprNumber(CPR_1).getId();
            bank.removeBankAccount(id_1);
        }catch(Exception error) {
            System.out.println("Already deleted.");
        }try{
            String CPR_2 = merchant.getCprNumber();
            String id_2 = bank.getAccountByCprNumber(CPR_2).getId();
            bank.removeBankAccount(id_2);
        } catch (Exception error) {
            System.out.println("Already deleted.");
        }
    }
}

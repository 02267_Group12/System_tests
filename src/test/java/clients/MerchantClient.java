package clients;

import dtu.ws.fastmoney.User;
import entities.MerchantTransaction;
import entities.PaymentData;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class MerchantClient {
    WebTarget baseUrl;
    String merchantPath = "merchant";

    public MerchantClient() {
        Client client = ClientBuilder.newClient();
        baseUrl = client.target("http://localhost:6002/");
    }

    public Response registerNewMerchant(User merchant){
        return baseUrl
                .path(merchantPath)
                .request()
                .post(Entity.entity(merchant, MediaType.APPLICATION_JSON));
    }

    public Response payToMerchant(MerchantTransaction merchantTransaction){
        return baseUrl
                .path(merchantPath)
                .path("payment")
                .request()
                .post(Entity.entity(merchantTransaction, MediaType.APPLICATION_JSON));
    }

    public Response getMerchantReport(String mid){
        return baseUrl
                .path(merchantPath)
                .path(mid)
                .path("report")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .get();
    }

    public Response getMerchantById(String mid){
        return baseUrl
                .path(merchantPath)
                .path(mid)
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .get();
    }
}

package clients;
import dtu.ws.fastmoney.User;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class ManagerClient {
    WebTarget baseUrl;
    String managerPath = "manager";
    String customerPath = "customer";
    String merchantPath = "merchant";

    public ManagerClient()
    {
        Client client = ClientBuilder.newClient();
        baseUrl = client.target("http://localhost:6003/");
    }

    public Response getCustomerAccount(String cid)
    {
        return baseUrl
                .path(managerPath).path(customerPath).path(cid)
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .get();
    }

    public Response updateCustomerAccount(String cid, User customer)
    {
        return baseUrl
                .path(managerPath).path(customerPath).path(cid)
                .request()
                .put(Entity.entity(customer, MediaType.APPLICATION_JSON));
    }

    public Response deleteCustomerAccount(String cid)
    {
        return baseUrl
                .path(managerPath).path(customerPath).path(cid)
                .request()
                .delete();
    }

    public Response getMerchantAccount(String mid)
    {
        return baseUrl
                .path(managerPath).path(merchantPath).path(mid)
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .get();
    }

    public Response updateMerchantAccount(String mid, User merchant)
    {
        return baseUrl
                .path(managerPath).path(merchantPath).path(mid)
                .request()
                .put(Entity.entity(merchant, MediaType.APPLICATION_JSON));
    }

    public Response deleteMerchantAccount(String mid)
    {
        return baseUrl
                .path(managerPath).path(merchantPath).path(mid)
                .request()
                .delete();
    }

    public Response getReport()
    {
        return baseUrl
                .path(managerPath).path("report")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .get();
    }


}

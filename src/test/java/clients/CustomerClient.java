package clients;

import dtu.ws.fastmoney.User;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class CustomerClient {
    WebTarget baseUrl;
    String port = "6001/";
    String customerPath = "customer";
    String tokensPath = "tokens";

    public CustomerClient() {
        Client client = ClientBuilder.newClient();
        baseUrl = client.target("http://localhost:"+port);
    }

    public Response registerCustomer(User user)
    {
        return baseUrl
                .path(customerPath)
                .request()
                .post(Entity.entity(user, MediaType.APPLICATION_JSON));
    }

    public Response getCustomerReport(String cid)
    {
        return baseUrl
                .path(customerPath).path(cid).path("report")
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .get();
    }

    public Response getCustomerById(String cid)
    {
        return baseUrl
                .path(customerPath).path(cid)
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .get();
    }

    public Response createTokens(String cid, int number)
    {
        return baseUrl
                .path(customerPath).path(cid).path(tokensPath)
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .post(Entity.entity(number, MediaType.APPLICATION_JSON));
    }

    public Response getTokensForCustomer(String cid)
    {
        return baseUrl
                .path(customerPath).path(cid).path(tokensPath)
                .request()
                .accept(MediaType.APPLICATION_JSON)
                .get();
    }
}
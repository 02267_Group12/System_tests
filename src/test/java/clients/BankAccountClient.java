package clients;

import dtu.ws.fastmoney.*;

import java.math.BigDecimal;

public class BankAccountClient {
    private static final String SERVICE_NAME = "Payment and report";
    static BankService bankService = new BankServiceService().getBankServicePort();

    public String createBankAccount(String firstName, String lastName, String cpr, BigDecimal amount){
        User user = new User();
        user.setCprNumber(cpr);
        user.setFirstName(firstName);
        user.setLastName(lastName);

        try {
            return bankService.createAccountWithBalance(user, amount);
        } catch (BankServiceException_Exception e) {
            return null;
        }
    }

    public boolean removeBankAccount(String id){
        try {
            bankService.retireAccount(id);
            return true;
        } catch (BankServiceException_Exception e) {
            return false;
        }
    }

    public Account getAccountByCprNumber(String cpr){
        try {
            return bankService.getAccountByCprNumber(cpr);
        } catch (BankServiceException_Exception e) {
            return null;
        }
    }

}

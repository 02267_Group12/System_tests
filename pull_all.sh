#!/bin/bash

set -e

PROJECTS=(
        AccountManagement_MS
        Manager_MS
        Payment_MS
        Customer_MS
        Merchant_MS
        ReportGeneration_MS
        TokenManagement_MS
)


pull_dir(){
	pushd "../$1"
	git reset --hard
	git pull
	popd
}

for project in ${PROJECTS[*]}; do
    pull_dir $project
done



Feature: Payment
  # Scenario as defined on piazza
  Scenario: Successful Payment
    Given the customer "Andrew" "Ryan" with CPR "061111-0089" has a bank account
    And the balance of the customer account is 1000
    And the customer is registered with DTUPay
    And the merchant "Frank" "Fontaine" with CPR "730521-0041" has a bank account
    And the balance of the merchant account is 2000
    And the merchant is registered with DTUPay
    And the customer has 2 valid tokens
    When the merchant initiates a payment for 10 kr by the customer
    Then the payment is successful
    And the balance of the customer at the bank is 990 kr
    And the balance of the merchant at the bank is 2010 kr
    And the customer report shows the transaction
    And the merchant report shows the transaction
    And the manager report shows the transaction


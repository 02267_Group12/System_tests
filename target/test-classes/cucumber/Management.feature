Feature: Management Scenarios
  Scenario: Updating and existing customer
    Given the customer "Andrew" "Ryan" with CPR "061111-0089" has a bank account
    And the balance of the customer account is 1000
    And the customer is registered with DTUPay
    When updating customer to "Salvador" "Raya" with CPR "061111-0089"
    Then customer "Salvador" "Raya" with CPR "061111-0089" is found

  Scenario: Updating and existing merchant
    Given the merchant "Andrew" "Ryan" with CPR "061111-0089" has a bank account
    And the balance of the merchant account is 1000
    And the merchant is registered with DTUPay
    When updating merchant to "Salvador" "Raya" with CPR "061111-0089"
    Then merchant "Salvador" "Raya" with CPR "061111-0089" is found

  Scenario: Deleting an existing customer and losing access to tokens
    Given the customer "Frank" "Fontaine" with CPR "730521-0041" has a bank account
    And the balance of the customer account is 1000
    And the customer is registered with DTUPay
    When deleting customer "Frank" "Fontaine" with CPR "730521-0041"
    Then customer "Frank" "Fontaine" with CPR "730521-0041" is not found
    And cannot access to tokens

  Scenario: Deleting an existing merchant
    Given the merchant "Frank" "Fontaine" with CPR "730521-0041" has a bank account
    And the balance of the merchant account is 1000
    And the merchant is registered with DTUPay
    When deleting merchant "Frank" "Fontaine" with CPR "730521-0041"
    Then merchant "Frank" "Fontaine" with CPR "730521-0041" is not found
#!/bin/bash

set -e

PROJECTS=(
        AccountManagement_MS
        Customer_MS
        Manager_MS
        Merchant_MS
        Payment_MS
        ReportGeneration_MS
        TokenManagement_MS
)

for project in ${PROJECTS[*]}; do
        echo "=========Building $project========="
        pushd "../$project"
        mvn clean
        chmod +x build.sh
        ./build.sh
        popd
done
